# BE Training Document

## 1. Setup Development Environment

### 1.1 Setup Visual Studio

    - Use draw.io to draw software architecure.
    - Use plantuml to draw use case diagram, class diagram, sequence diagram and state diagram.
    - Use markdown to write document in BE Team
    - Use Visual Studio to write document using markdown and plantuml

#### Install Plantuml extention and Markdown extention

    - PlantUML
    - Markdown

![image info](./images/install_plantuml_and_markdown_extention.png)

#### Setup Plantuml server

    docker pull plantuml/plantuml-server
    docker run -d -p 8080:8080 plantuml/plantuml-server:jetty

#### Setup Plantuml extention configuration

    "plantuml.server": "http://127.0.0.1:8080",
    "plantuml.render": "PlantUMLServer",

![image info](./images/setting_plantuml_extention_configuration.png)

## 2. Setup Java and Maven

### 2.0 Version

    Java 8 and Maven 3.6.3

### 2.1 Setup m2/settings.xml

```
  <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <servers>
  <server>
    <id>kzone_uat</id>
    <username>{USERNAME}</username>
    <password>{PASSWORD}</password>
  </server>
  <server>
    <id>itl_dev</id>
    <username>{USERNAME}</username>
    <password>{PASSWORD}</password>
  </server>
  </servers>
  <profiles>
    <profile>
      <id>kzone_uat</id>
    <repositories>
          <repository>
            <id>kzone_uat</id>
            <name>kzone_uat</name>
            <url>http://pay-nexus.inspirelab.io:8081/repository/kz-uat</url>
          </repository>
      </repositories>
    </profile>
    <profile>
      <id>itl_dev</id>
      <repositories>
          <repository>
            <id>itl_uat</id>
            <name>itl_uat</name>
            <url>http://pay-nexus.inspirelab.io:8081/repository/itl-dev</url>
          </repository>
      </repositories>
    </profile>
  </profiles>
  </settings>
```

Adding the property in the profile to deploy on Jenkin

```
  <properties>
       <altDeploymentRepository>{SERVER_ID}::default::{NEXUS_URL}</altDeploymentRepository>
  </properties>
```

### 2.2 Maven commands

```
  Building without testing
  mvn -P {ACTIVE_PROFILES}  clean install -DskipTests

```

### 2.4 Choosing Active profiles in Inteliji

![image info](./images/active_profiles_in_iteliji.png)

### 2.3 Setup

## 3. Setup Intellij

## 4. Setup Credentials

## 5. Run the demo project

## 6. Create new project

### 6.1 Software architecture

### 6.2 Source code architecture

## 7. How to make Http API

## 8. How to make Grpc API

## 9. How to make Kafka consumer and producer

## 10. How to make ActiveMQ consumer and producer

## 11. How to use Genuid

## 12. How to connect to Database Service

## 13. How to initialize new table with non-sharding, sharding by murmur3 or daily

## 14. How to change schema table

## 15. How to handle new feature

## 16. How to handle task

## 17. How to handle bug

## 18. How to deploy on development environment

## 19. BE Development Flow

- https://drive.mindmup.com/map/1GKbanIYcprCvfn1ZXr5NcpCdQjAJgVu0
  ![image info](./images/be_development_flow.png)
