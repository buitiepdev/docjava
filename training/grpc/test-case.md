1. Test case: Register with existed username 
Input: username registered
Excepted: throw ExistedUserException 

2. Test case: Register and create wallet success 
Input: username, email, password, phone_number
Excepted: Create user and wallet success

3. Test case: Check user deposit 
Input: username (not registered), deposit amount
Excepted: throw UserNotFoundException 

4. Test case: Deposit balance success 
Input: username (registered), deposit amount
Excepted: Deposit balance success

5. Test case: Check user withdraw 
Input: username (not registered), amount 
Excepted: throw UserNotFoundException 

6. Test case: Check balance withdraw not enough
Input: username, user balance less than withdraw amount .
Excepted: throw NotEnoughBalanceException

7. Test case: Check balance withdraw enough
Input: username, user balance greater or equal than withdrawal amount.
Excepted: Withdraw balance enough

8. Test case: Withdraw balance success 
Input: username (registered), user balance greater or equal than withdrawal amount.
Excepted: Withdraw balance success.

9. Test case: Check user transfer 
Input: usernameSender or usernameReceiver (not registered), amount transfer.
Excepted: throw UserNotFoundException 

10. Test case: Check balance of sender not enough (Transfer)
Input: usernameSender (registered), usernameReceiver (registered), transfer amount greater than sender balance.
Excepted: throw NotEnoughBalanceException

11. Test case: Check balance of sender enough (Transfer)
Input: usernameSender (registered), usernameReceiver (registered), transfer amount less or equal than sender balance.
Excepted: Enough transfer balance

12. Test case: Transfer success
Input: usernameSender (registered), usernameReceiver (registered), transfer amount less than sender balance.
Excepted: Transfer success 

