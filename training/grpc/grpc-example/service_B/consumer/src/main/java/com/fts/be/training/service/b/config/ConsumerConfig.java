package com.fts.be.training.service.b.config;

import com.fts.be.training.service.b.listener.DemoListener;
import com.fts.be.training.service.b.representation.constant.KafkaConstant;
import com.fts.be.training.service.b.representation.msg.KafkaMessage;
import com.fts.common.configuration.common.response.KafkaConfig;
import com.fts.common.configuration.sdk.config.annotation.ConfigKafkaResource;
import com.fts.common.extensions.annotation.EnableKafkaTracing;
import com.fts.common.extensions.consumer.KafkaConsumerConfigInit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;

@Configuration
@EnableKafkaTracing
public class ConsumerConfig {
    @ConfigKafkaResource(value = "kafka_topic", scope = "kafka_demo")
    public static KafkaConfig kafkaDemo;

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, KafkaMessage> kafkaDemoMessageContainerFactory() {
        return KafkaConsumerConfigInit.getKafkaListenerContainerFactory(kafkaDemo.getTopicConfig(KafkaConstant.DEMO_TOPIC), KafkaMessage.class);
    }

    @Bean
    public DemoListener demoListener() {
        return new DemoListener();
    }
}
