package com.fts.be.training.service.b.listener;

import com.fts.be.training.service.b.representation.msg.KafkaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public class DemoListener {
    private static final Logger logger = LoggerFactory.getLogger(DemoListener.class);
    @KafkaListener(topics = "#{'${DemoTopic}'.split(',')}", containerFactory = "kafkaDemoMessageContainerFactory")
    public void freezeTransactionListener(KafkaMessage message, Acknowledgment acknowledgment)  {
        logger.info("Receive message: " + message);
        acknowledgment.acknowledge();
    }
}
