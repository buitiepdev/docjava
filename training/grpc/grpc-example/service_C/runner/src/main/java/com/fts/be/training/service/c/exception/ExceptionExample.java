package com.fts.be.training.service.c.exception;

public class ExceptionExample extends Exception {
    public ExceptionExample() {
    }

    public ExceptionExample(String message) {
        super(message);
    }

    public ExceptionExample(String message, Throwable cause) {
        super(message, cause);
    }
}
