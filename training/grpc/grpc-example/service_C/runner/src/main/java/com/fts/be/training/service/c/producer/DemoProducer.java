package com.fts.be.training.service.c.producer;
import com.fts.be.training.service.b.representation.msg.KafkaMessage;
import com.fts.common.extensions.producer.KafkaProducer;
import org.springframework.kafka.core.KafkaTemplate;

public class DemoProducer {
    private final KafkaProducer kafkaProducer;
    private final KafkaTemplate<String, KafkaMessage> kafkaDemoMessageTemplate;

    public DemoProducer(KafkaProducer kafkaProducer, KafkaTemplate<String, KafkaMessage> kafkaDemoMessageTemplate) {
        this.kafkaProducer = kafkaProducer;
        this.kafkaDemoMessageTemplate = kafkaDemoMessageTemplate;
    }

    public boolean sendDemoMessage(KafkaMessage message) {
        return kafkaProducer.sendMessageSync(kafkaDemoMessageTemplate, message);
    }
}
