package com.fts.be.training.service.c.handler;

import com.fts.be.training.service.c.exception.ExceptionExample;
import com.fts.common.api.common.payload.message.BaseMessageResponse;
import com.fts.common.api.server.exception.ExceptionHandler;
import com.fts.common.api.server.exception.annotation.HandlerException;
import com.fts.common.api.server.exception.annotation.HandlerExceptionClass;

@HandlerExceptionClass
public class ExceptionHandlerExample extends ExceptionHandler {
    @HandlerException
    public BaseMessageResponse exampleException(ExceptionExample e) {
        String message = e.getMessage() != null ? e.getMessage() : e.toString();
        int code = -1;
        int status = 500;
        return new BaseMessageResponse(message, code, status);
    }
}
