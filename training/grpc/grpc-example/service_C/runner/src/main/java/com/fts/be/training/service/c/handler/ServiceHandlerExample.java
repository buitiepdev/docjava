package com.fts.be.training.service.c.handler;



import com.fts.be.training.service.b.representation.msg.KafkaMessage;
import com.fts.be.training.service.c.presentation.constant.MethodConstant;
import com.fts.be.training.service.c.presentation.request.BaseGrpcRequest;
import com.fts.be.training.service.c.presentation.response.BaseGrpcResponse;
import com.fts.be.training.service.c.producer.DemoProducer;
import com.fts.be.training.service.c.representation.request.BaseHttpRequest;
import com.fts.be.training.service.c.representation.response.BaseHttpResponse;
import com.fts.common.api.common.constant.Protocol;
import com.fts.common.api.common.payload.data.DataResponse;
import com.fts.common.api.server.service.ServiceHandler;
import com.fts.common.api.server.service.annotation.HandlerService;
import com.fts.common.api.server.service.annotation.HandlerServiceClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@HandlerServiceClass
public class ServiceHandlerExample extends ServiceHandler {
    private static final Logger logger = LoggerFactory.getLogger(ServiceHandlerExample.class);
    private final DemoProducer producer;

    public ServiceHandlerExample(DemoProducer producer) {
        this.producer = producer;
    }

    @HandlerService(method = MethodConstant.METHOD_GET)
    public DataResponse<BaseGrpcResponse> getExample(BaseGrpcRequest request) {
        Integer input = request.getInput();
        logger.info(input.toString());
        BaseGrpcResponse repResponse = new BaseGrpcResponse("Test get Successfully in Grpc", input + 1);
        producer.sendDemoMessage(new KafkaMessage("Message Get Request"));
        return new DataResponse<>(repResponse);
    }

    @HandlerService(method = MethodConstant.METHOD_POST)
    public DataResponse<BaseGrpcResponse> postExample(BaseGrpcRequest request) {
        Integer input = request.getInput();
        logger.info(input.toString());
        BaseGrpcResponse repResponse = new BaseGrpcResponse("Test post Successfully in Grpc", input + 1);
        producer.sendDemoMessage(new KafkaMessage("Message Post Request"));
        return new DataResponse<>(repResponse);
    }
    @HandlerService(path = "/example/get")
    public DataResponse<BaseHttpResponse> exampleGet(BaseHttpRequest request) {
        BaseHttpResponse repResponse = new BaseHttpResponse("Test", 3);
        return new DataResponse<BaseHttpResponse>(repResponse);
    }

    @HandlerService(path = "/example/post", proto = Protocol.POST)
    public DataResponse<BaseHttpResponse> examplePost(BaseHttpRequest request) {
        BaseHttpResponse repResponse = new BaseHttpResponse(request.getMessage(), request.getAge());
        return new DataResponse<BaseHttpResponse>(repResponse);
    }
}
