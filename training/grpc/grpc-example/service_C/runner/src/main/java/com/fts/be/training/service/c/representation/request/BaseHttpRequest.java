package com.fts.be.training.service.c.representation.request;

import com.fts.common.api.common.payload.data.impl.BaseDataRequest;
import com.google.gson.annotations.SerializedName;

public class BaseHttpRequest extends BaseDataRequest {
    private static final long serialVersionUID = 1L;

    @SerializedName("message")
    private String message;
    @SerializedName("age")
    private Integer age;

    public BaseHttpRequest(String message, Integer age) {
        this.message = message;
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "BaseRequest{" +
                ", message='" + message + '\'' +
                "} ";
    }

}