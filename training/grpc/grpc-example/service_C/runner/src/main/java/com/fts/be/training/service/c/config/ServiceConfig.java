package com.fts.be.training.service.c.config;

import com.fts.be.training.service.c.handler.ServiceHandlerExample;
import com.fts.be.training.service.c.producer.DemoProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfig {
    @Bean
    public ServiceHandlerExample serviceHandlerExample(DemoProducer producer) {
        return new ServiceHandlerExample(producer);
    }
}
