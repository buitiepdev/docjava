package com.fts.be.training.service.c.sdk.impl;

import com.fts.be.training.service.c.presentation.response.BaseGrpcResponse;
import com.fts.common.api.common.payload.data.DataResponse;

public interface GrpcSdk {
    DataResponse<BaseGrpcResponse> getExample(String message, Integer data);
    DataResponse<BaseGrpcResponse> postExample(String message, Integer data);
}
