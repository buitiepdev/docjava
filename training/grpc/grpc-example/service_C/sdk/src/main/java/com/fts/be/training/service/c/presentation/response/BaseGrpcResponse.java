package com.fts.be.training.service.c.presentation.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseGrpcResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    @SerializedName("message")
    private String message;
    @SerializedName("input")
    private Integer input;

    public BaseGrpcResponse(String message, Integer input) {
        this.message = message;
        this.input = input;
    }

    public String getMessage() {
        return message;
    }

    public Integer getInput() {
        return input;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setInput(Integer input) {
        this.input = input;
    }
    @Override
    public String toString() {
        return "BaseResponse { " +
                " message='" + message + '\'' +
                ", data=" + input +
                '}';
    }
}
