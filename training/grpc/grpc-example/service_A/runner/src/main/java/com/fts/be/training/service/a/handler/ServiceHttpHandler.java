package com.fts.be.training.service.a.handler;

import com.fts.be.training.service.a.representation.request.BaseHttpRequest;
import com.fts.be.training.service.a.representation.response.BaseHttpResponse;


import com.fts.be.training.service.c.presentation.response.BaseGrpcResponse;
import com.fts.be.training.service.c.sdk.ClientGrpcSdk;
import com.fts.common.api.common.constant.Protocol;
import com.fts.common.api.common.payload.data.DataResponse;
import com.fts.common.api.server.service.ServiceHandler;
import com.fts.common.api.server.service.annotation.HandlerService;
import com.fts.common.api.server.service.annotation.HandlerServiceClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@HandlerServiceClass
public class ServiceHttpHandler extends ServiceHandler {
    private static final Logger logger = LoggerFactory.getLogger(ClientGrpcSdk.class);
    private final ClientGrpcSdk client = new ClientGrpcSdk("localhost", 8081);
    @HandlerService(path = "/example/get")
    public DataResponse<BaseHttpResponse> exampleGet(BaseHttpRequest request) {
        BaseHttpResponse repResponse = new BaseHttpResponse("Test", 3);
        DataResponse<BaseGrpcResponse> response = client.getExample(repResponse.getMessage(), repResponse.getAge());
        repResponse.setMessage(response.getData().getMessage());
        repResponse.setAge(response.getData().getInput());
        return new DataResponse<BaseHttpResponse>(repResponse);
    }

    @HandlerService(path = "/example/post", proto = Protocol.POST)
    public DataResponse<BaseHttpResponse> examplePost(BaseHttpRequest request) {
        BaseHttpResponse repResponse = new BaseHttpResponse(request.getMessage(), request.getAge());
        DataResponse<BaseGrpcResponse> response = client.postExample(repResponse.getMessage(), repResponse.getAge());
        System.out.print(response.getData().getMessage());
        return new DataResponse<BaseHttpResponse>(repResponse);
    }
}
