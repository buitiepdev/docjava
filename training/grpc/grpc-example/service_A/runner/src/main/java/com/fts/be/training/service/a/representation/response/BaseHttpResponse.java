package com.fts.be.training.service.a.representation.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseHttpResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("message")
    private String message;

    @SerializedName("age")
    private Integer age;

    public BaseHttpResponse(String message, Integer age) {
        this.message = message;
        this.age = age;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "BaseHttpResponse {" +
                "message='" + message + '\'' +
                ", age=" + age +
                '}';
    }
}
