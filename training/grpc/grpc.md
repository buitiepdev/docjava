# DocJava

## RPC

RPC – (Remote Procedure Call) là một mô hình kỹ thuật mạng hay còn được biết đến là cơ chế giao tiếp giữa hai tiến trình.
cho phép ứng dụng gọi các phương thức hoặc thủ tục từ xa như việc gọi các phương thức cục bộ, mà không cần quan tâm đến chi tiết của giao tiếp mạng. RPC giúp tạo ra sự tương tác giữa các thành phần ứng dụng trên các máy tính khác nhau.

### Architecture

- Một ứng dụng Client- Server theo cơ chế RPC được xây dựng gồm các phần:

#### Client Application

- The Client application initiates calls to remote methods.
- Each call triggers a procedure in the Client Stub.

#### Client Stub

- Part of the RPC library, containing methods corresponding to remote procedures.
- The Client Stub provides a local interface for the Client application to call remote methods without needing to know network communication details.
- When called, the Client Stub creates a remote method call message and sends it through the RPC Runtime.

#### RPC Runtime

- A component that manages network communication between the Client and Server machines.
- Responsible for message transmission, encoding, decoding, and other related tasks.
- The RPC Runtime handles packet transmission between the Client Stub and Server Stub.

### Network Communication

- The RPC Runtime uses transport protocols (e.g., TCP, UDP) to send and receive messages over the network.
- Messages are encapsulated, encoded, and sent from the Client machine to the Server machine.

#### Server Stub

- Part of the RPC library, containing remote methods that are implemented on the server.
- Upon receiving a message from the Client, the Server Stub identifies the corresponding remote method and invokes it.
- After execution, the Server Stub returns the result through the RPC Runtime.

#### Server Application

- The Server application performs processing logic for remote methods called from the Client.
- The Server application can reside on the same or a different computer compared to the Client application.

## Workflow of RPC (Remote Procedure Call):

1. Initializing Remote Call:

   - The process begins when the Client application needs to invoke a remote method.
   - This call can be a function or method call, and the Client application does not need to be aware of the distinction.

2. Client Stub:

   - The Client Stub is part of the RPC library and provides an interface to call remote methods similar to calling local methods.
   - The Client application calls remote methods through the Client Stub.
   - The Client Stub packages information about the method and its parameters into a call message.

3. Message Transmission:

   - The call message from the Client application is transmitted over the network to the Server application using network communication mechanisms (e.g., TCP, UDP).

4. RPC Runtime on Server:

   - The RPC Runtime on the Server side receives the call message and forwards it to the Server Stub.

5. Server Stub:

   - The Server Stub is part of the RPC library and contains implemented remote methods on the server.
   - The Server Stub decodes the call message, identifies the corresponding method and parameters.

6. Executing Remote Method:

   - The Server Stub invokes the requested remote method and performs the required processing.
   - The result of the remote method is returned by the Server Stub.

7. Returning Result:

   - The result of the remote method is packaged into a result message and sent back over the network to the Client side.

8. RPC Runtime on Client:

   - The RPC Runtime on the Client side receives the result message and forwards it to the Client Stub.

9. Client Stub Processing Result:
   - The Client Stub decodes the result message and returns the result to the Client application.

This process enables the Client application to call remote methods seamlessly and efficiently, while network communication and complex logic processing are managed by the RPC system.

## What's the difference between RPC and REST?

Remote Procedure Call (RPC) and Representational State Transfer (REST) are two distinct architectural styles used in API design. APIs (Application Programming Interfaces) serve as mechanisms that facilitate communication between software components by defining a set of protocols and definitions. This allows developers to leverage existing or third-party components to perform various functions without having to build everything from scratch.

### What are the similarities between RPC and REST?

1. API Design: are approaches to designing APIs (Application Programming Interfaces) that enable communication between distributed applications or services.
2. Abstraction: Both RPC and REST abstract away the lower-level details of network communication, allowing developers to focus on defining and using functions or resources.
3. Communication: RPC and REST both use HTTP as the underlying protocol for communication. They utilize HTTP methods such as GET, POST, PUT, and DELETE to perform actions on remote resources.
4. Message Formats: Both RPC and REST support common message formats such as JSON (JavaScript Object Notation) and XML (eXtensible Markup Language) for data exchange. JSON is often preferred for its readability and ease of use.
5. Cross-Language Compatibility: Developers can implement both RPC and REST APIs in various programming languages. As long as the API conforms to the specified interface and communication standards, the rest of the code can be written in any supported language.
6. Data Exchange: Both RPC and REST facilitate data exchange between client and server applications, enabling them to interact seamlessly over a network.

### Architecture principles: RPC vs. REST

- RPC principles

1. Remote invocation
   An RPC call is made by a client to a function on the remote server as if it were called locally to the client.

2. Passing parameters
   The client typically sends parameters to a server function, much the same as a local function.

3. Stubs
   Function stubs exist on both the client and the server. On the client side, it makes the function call. On the server, it invokes the actual function.

- REST principles

1. Client-server
   The client-server architecture of REST decouples clients and servers. It treats them each as independent systems.

2. Stateless
   The server keeps no record of the state of the client between client requests.

3. Cacheable
   The client or intermediary systems may cache server responses based on whether a client specifies that the response may be cached.

4. Layered system
   Intermediaries can exist between the client and the server. Both client and server have no knowledge of them and operate as if they were directly connected.

5. Uniform interface
   The client and server communicate via a standardized set of instructions and messaging formats with the REST API. Resources are identified by their URL, and this URL is known as a REST API endpoint.

### How they work: RPC vs. REST

In Remote Procedure Call (RPC), the client uses HTTP POST to call a specific function by name. Client-side developers must know the function name and parameters in advance for RPC to work.

In REST, clients and servers use HTTP verbs like GET, POST, PATCH, PUT, DELETE, and OPTIONS to perform options. Developers only need to know the server resource URLs and don't have to be concerned with individual function names.

### Key differences: RPC vs. REST

- Time of development
  RPC was developed in the late 1970s and early 1980s, while REST was a term first coined by computer scientist Roy Fielding in 2000.

- Operational format
  A REST API has a standardized set of server operations because of HTTP methods, but RPC APIs don’t. Some RPC implementations provide a framework for standardized operations.

- Data passing format
  REST can pass any data format and multiple formats, like JSON and XML, within the same API.

However, with RPC APIs, the data format is selected by the server and fixed during implementation. You can have specific JSON RPC or XML RPC implementations, and the client has no flexibility.

State
In the context of APIs, stateless refers to a design principle where the server does not store any information about the client's previous interactions. Each API request is treated independently, and the server does not rely on any stored client state to process the request.

REST systems must always be stateless, but RPC systems can be stateful or stateless, depending on design.

### When to use: RPC vs. REST

Remote Procedure Call (RPC) is typically used to call remote functions on a server that require an action result. You can use it when you require complex calculations or want to trigger a remote procedure on the server, with the process hidden from the client.
VD: Transfer money from one account to another on a remote banking system

A REST API is typically used to perform create, read, update, and delete (CRUD) operations on a data object on a server. This makes REST APIs a good fit for cases when server data and data structures need to be exposed uniformly.
VD:

- Add a product to a database
- Retrieve the contents of a music playlist

## gRPC

gRPC (gRPC Remote Procedure Call) is a technology designed to facilitate remote communication between distributed applications through the use of remote procedure calls (RPCs). It was developed by Google and is widely used to build high-performance, reliable, and flexible services and applications. Below is the core architecture of gRPC.

### Architecture gRPC

1. IDL (Interface Definition Language)

- gRPC utilizes an IDL to define services and messages that clients and servers can use to communicate. The IDL defines how remote procedures are called and how data is transmitted over the network.

2. Protobuf (Protocol Buffers): Protobuf, a binary data format provided by Google, is used to define the data structure of messages in gRPC. This helps reduce network traffic and increases performance speed.

3. Server: A gRPC server implements the services defined in the IDL. When the server receives requests from clients through remote procedure calls, it performs the corresponding processing and returns the results to the client.

4. Client: A gRPC client is constructed based on the IDL to invoke remote procedures implemented on the server. When the client calls a remote procedure, it generates a request and sends it to the server, then waits for a response from the server.

5. RPC Channel: This is an abstract layer for sending and receiving gRPC messages over the network. The channel manages connections and data transmission between the client and server. Different protocols like HTTP/2 can be used to implement gRPC channels.

6. Serialization and Deserialization: Data in gRPC is converted into binary format using Protobuf when sent from the client to the server and vice versa. This conversion process is known as serialization and deserialization.

7. Middleware and Interceptors: gRPC supports middleware and interceptors to perform common functions such as authentication, logging, error management, and more. This helps separate application logic from unrelated functionalities.

8. Streaming: gRPC supports both client-to-server (client streaming) and server-to-client (server streaming) streaming, as well as bidirectional streaming. This allows real-time data transmission in both directions.

### gRPC life cycle

1. IDL and Stub Generation(RPC Call Initiation):

- The process starts with defining the services and methods using an Interface Definition Language (IDL), which outlines the available remote procedures and their parameters.
- Stub generation: The IDL is used to generate client and server stubs. The client stub contains proxy methods that mirror the remote methods on the server, while the server stub contains the actual implementations of those methods.

2. Client-Side Invocation :

- The client-side code invokes a remote procedure call using the generated client stub.
- The client marshals the arguments (parameters) of the procedure into a suitable format, often serializing them (e.g., to JSON or Protocol Buffers).

3. Request Transmission (Sending Request):

- The client transmits the serialized request to the server over the network.
- The request is typically sent using a network protocol such as HTTP, gRPC, or other custom protocols.

4. Server-Side Processing:

- The server receives the incoming request.
- The server stub on the server processes the request, deserializes the incoming data, and locates the corresponding method implementation.

5. Method Execution:

- The server-side method is executed, performing the required computations or operations based on the received parameters.

6. Response Generation:

- The method execution on the server generates a response containing the result of the remote procedure call.
- The response may include return values, error codes, or other relevant data.

7. Response Transmission:

- The server serializes the response data into an appropriate format.

8. Client-Side Handling:

- The client receives the serialized response from the server.

9. Response Processing:

- The client deserializes the response and extracts the relevant information, such as return values or errors.

10. Completion of RPC:

- The client-side code continues its execution, using the received results from the remote procedure call.

## Các thuật ngữ khác

### Synchronous vs. asynchronous

- Synchronous RPC: In synchronous RPC, the client sends a request to the server and waits for a response before proceeding with further actions. The client's execution is blocked until it receives the response from the server.

- Asynchronous RPC: In asynchronous RPC, the client sends a request to the server and continues with its own operations without waiting for a response. The client periodically checks for a response or registers a callback function to be executed when the response arrives.

### Unary RPC (no streaming)

- Unary RPC in gRPC is a simple and synchronous type of remote procedure call between a client and a server. In Unary RPC, the client sends a request to the server and waits for a response. This is similar to calling a regular function in a programming language, where you pass arguments and wait for a return result.
- How Unary RPC works:

1. Client Sends Request: The client sends a remote procedure call request to the server through a pre-defined method.

2. Server Processes Request: The server receives the request from the client and performs the corresponding processing for the request. After processing is complete, the server sends back a response result to the client.

3. Client Receives Response: The client waits for the response from the server. When the server finishes processing and returns a result, the client receives the response.

4. Completion of RPC Call: After receiving the response from the server, the remote procedure call completes, and the client can continue with other tasks.

### Steaming

- Server Streaming RPC:

A server-streaming RPC is a type of remote procedure call where the client sends a single request to the server, and the server responds with a stream of messages. This means that the server sends multiple messages back to the client in response to the single client request. Once the server has sent all of its messages, it also sends its status details (status code and optional status message) and optional trailing metadata to the client. At this point, the processing on the server side is completed. The client, on the other hand, completes its processing once it has received all of the messages from the server.

- Client Streaming RPC:

A client-streaming RPC is another type of remote procedure call where the client sends a stream of messages to the server instead of a single message. The server then responds with a single message, along with its status details and optional trailing metadata. The server may send this response after it has received all of the client's messages, but it's not strictly necessary. The key difference here is that the client is the one streaming messages to the server.

- Bidirectional Streaming RPC:

In a bidirectional streaming RPC, the client initiates the call by invoking the method, and the server receives the client metadata, method name, and deadline. The server can choose to respond with its initial metadata or wait for the client to start streaming messages. Both the client and server can independently read and write messages in any order since the two streams are independent. This allows for flexible communication patterns. For example, the server might wait until it has received all of the client's messages before sending its own messages, or they can engage in a back-and-forth "ping-pong" style of communication.

In summary, gRPC supports various streaming modes: server streaming, client streaming, and bidirectional streaming. These modes enable efficient communication between client and server, allowing them to exchange multiple messages or data streams in a flexible and asynchronous manner.

### Deadlines/Timeouts

- Deadlines: A deadline is a maximum amount of time that a client is willing to wait for a response from the server. It is set by the client when making an RPC request. The deadline represents the point in time by which the client expects to receive a response. If the server doesn't respond within the specified deadline, the client can choose how to handle the situation, such as retrying the request, reporting an error, or taking other appropriate actions.

- Timeouts: Timeouts are related to deadlines and represent the duration of time the client is willing to wait for a response. While deadlines specify an absolute point in time, timeouts specify the maximum duration for waiting. When a timeout is reached, if the server has not responded, the client considers the request as unsuccessful and can handle it accordingly.

### RPC Termination

- Terminating an RPC call in gRPC occurs when the communication process between the client and server is completed. An RPC call can terminate either successfully or unsuccessfully, and the termination process can be initiated from both sides. Here are some scenarios related to the termination of an RPC call:

1. Successful Completion: An RPC call is successfully completed when the client receives a response from the server. The response may contain the necessary data or indicate the successful execution of the request.

2. Request Processing Error: An RPC call may terminate with an error when the server is unable to process the client's request. This error could be due to issues with data processing, system errors, or any other reasons.

3. Deadline Exceeded: The client can set a deadline for the RPC call, and if the deadline expires, the call will be terminated and an "Deadline Exceeded" error message will be returned.

- The process of terminating an RPC call is important to ensure that remote tasks are performed accurately and efficiently, and to manage resources and handle errors effectively.

### Cancelling an RPC

- Cancelling an RPC in gRPC refers to the process of prematurely ending an ongoing remote procedure call between a client and a server. This cancellation can be initiated by either the client or the server.

### Metadata

- Metadata in gRPC refers to additional contextual information that can be attached to an RPC (Remote Procedure Call) to provide extra details or instructions for the call's processing. Metadata can be used to carry various types of information between the client and the server during an RPC interaction. It helps in enhancing communication and enabling features beyond just the raw data being exchanged.

- In gRPC, metadata consists of key-value pairs, where each key is a string and the corresponding value can be of various types, such as strings, binary data, or other serializable objects. Metadata is used for purposes like authentication, tracing, monitoring, and custom headers.

## What are the different between RPC and gRPC

RPC (Remote Procedure Call) and gRPC (gRPC Remote Procedure Call) are two approaches to designing application interfaces (APIs) for remote communication between distributed components. Here are some comparisons between RPC and gRPC:

1. Basic Principles:

   - RPC: RPC is a general model for calling remote functions between distributed components. It does not define a specific standard for how functions are called or how data is transmitted.
   - gRPC: gRPC is a specific form of RPC developed by Google, using Protobuf as the data format and HTTP/2 as the transport protocol. It provides a high-performance, reliable, and flexible remote communication mechanism.

2. Data Format:

   - RPC: Does not define a specific data format, can use JSON, XML, or other formats.
   - gRPC: Uses Protobuf (Protocol Buffers) as the standardized data format. Protobuf optimizes data size and enhances performance.

3. Transport Protocol:

   - RPC: Can use various protocols such as HTTP, TCP, UDP, etc.
   - gRPC: Uses HTTP/2 as the default transport protocol, supporting multiplexing and data compression for optimized communication performance.

4. Code Generation:

   - RPC: Often requires manual code writing for both client and server to implement remote functions.
   - gRPC: Provides automatic code generation for both client and server from IDL and Protobuf format.

5. Streaming:

   - RPC: Can support streaming through some protocols, but there is no standardized way to implement streaming.
   - gRPC: Supports streaming through different types of RPC: server streaming, client streaming, and bidirectional streaming.

6. Error Handling:
   - RPC: No specific standard for error handling and status.
   - gRPC: Defines standard status codes and provides error handling mechanisms based on status codes.

In summary, gRPC is a more advanced variant of RPC, utilizing Protobuf and HTTP/2 to provide high performance and powerful features for remote communication. If you're looking for a high-performance remote communication solution with automated tools available, gRPC could be a good choice.

# Sequence

![image info](./images/sequence.png)
