package com.fts.be.training.service.b.runner;

import com.fts.be.training.service.b.config.ConsumerConfig;
import com.fts.common.configuration.sdk.config.InitConfiguration;
import com.fts.common.configuration.sdk.exception.GetConfigException;
import com.fts.common.configuration.sdk.exception.InvalidFieldException;
import com.fts.common.configuration.sdk.exception.NotFoundException;
import com.fts.ioz.common.ioc.SpringApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

public class ConsumerRunner {
    public static void main(String[] args) throws NotFoundException, IOException, InvalidFieldException, GetConfigException, IllegalAccessException {
        InitConfiguration initConfiguration = new InitConfiguration("demo.consumer.application.properties");
        initConfiguration.setConfigurationAnnotation(ConsumerConfig.class);
        AnnotationConfigApplicationContext context = initConfiguration.getContext();
        context.refresh();
        SpringApplicationContext.setSharedApplicationContext(context);
    }
}
