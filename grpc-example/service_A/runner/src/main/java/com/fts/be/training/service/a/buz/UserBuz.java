package com.fts.be.training.service.a.buz;

import com.fts.be.training.service.a.representation.request.BaseHttpRequest;
import com.fts.be.training.service.a.representation.response.BaseHttpResponse;

public interface UserBuz {
    BaseHttpResponse register(BaseHttpRequest request);
}
