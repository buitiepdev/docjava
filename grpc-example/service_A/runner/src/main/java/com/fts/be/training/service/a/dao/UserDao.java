package com.fts.be.training.service.a.dao;

import com.fts.be.training.service.a.entity.UserEntity;

public interface UserDao {
    void register(UserEntity userEntity);
}
