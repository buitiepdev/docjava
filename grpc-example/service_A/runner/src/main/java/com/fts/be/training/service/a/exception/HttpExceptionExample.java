package com.fts.be.training.service.a.exception;

public class HttpExceptionExample extends Exception{
    public HttpExceptionExample() {
    }

    public HttpExceptionExample(String s) {
        super(s);
    }

    public HttpExceptionExample(String s, Throwable throwable) {
        super(s, throwable);
    }
}
