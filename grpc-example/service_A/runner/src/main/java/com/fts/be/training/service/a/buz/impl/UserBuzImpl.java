package com.fts.be.training.service.a.buz.impl;

import com.fts.be.training.service.a.buz.UserBuz;
import com.fts.be.training.service.a.entity.UserEntity;
import com.fts.be.training.service.a.representation.request.BaseHttpRequest;
import com.fts.be.training.service.a.representation.response.BaseHttpResponse;
import com.fts.be.training.service.a.service.UserService;
import com.fts.ioz.common.genuid.GenUID;
import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserBuzImpl implements UserBuz {
    private static final Logger logger = LoggerFactory.getLogger(UserBuzImpl.class);
    private final UserService userService;
    private final GenUID genUID;

    public UserBuzImpl(UserService userService, GenUID genUID) {
        this.userService = userService;
        this.genUID = genUID;
    }
    @Override
        public BaseHttpResponse register(BaseHttpRequest request) {
        Long userIdGen = genUID.getUID();
        // UserEntity user = userService.register(userIdGen, request.getUsername(), request.getEmail(), request.getPhone(), request.getPassword());
        BaseHttpResponse response = new BaseHttpResponse();
        // response.setUserId(user.getUserId());
        // response.setEmail(user.getEmail());
        response.setUserId(userIdGen);
        return response;
    }

}
