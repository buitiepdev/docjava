package com.fts.be.training.service.a.service.impl;

import com.fts.be.training.service.a.dao.UserDao;
import com.fts.be.training.service.a.entity.UserEntity;
import com.fts.be.training.service.a.service.UserService;
import com.fts.dbservice.sdk.anotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Transactional
    @Override
    public UserEntity register(Long userId, String username, String email, String phone, String password) {
        UserEntity user = new UserEntity();
        user.setUserId(userId);
        user.setUsername(username);
        user.setEmail(email);
        user.setPhone(phone);
        user.setPassword(password);
        logger.info(username, userId);
        this.userDao.register(user);
        return user;
    }
}
