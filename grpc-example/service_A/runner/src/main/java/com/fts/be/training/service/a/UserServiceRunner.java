package com.fts.be.training.service.a;

import com.fts.be.training.service.a.buz.UserBuz;
import com.fts.be.training.service.a.config.UserServiceConfig;
import com.fts.be.training.service.a.handler.ExceptionHttpHandler;
import com.fts.be.training.service.a.handler.UserServiceHandler;
import com.fts.common.api.server.CommonServer;
import com.fts.common.configuration.sdk.config.InitConfiguration;
import com.fts.ioz.common.ioc.SpringApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.concurrent.TimeUnit;

public class UserServiceRunner {

    public static void main(String[] args) throws Exception {
        InitConfiguration initConfiguration = new InitConfiguration();
        initConfiguration.setConfigurationAnnotation(UserServiceConfig.class);
        AnnotationConfigApplicationContext context = initConfiguration.getContext();
        context.refresh();
        SpringApplicationContext.setSharedApplicationContext(context);
        CommonServer commonServer = new CommonServer(UserServiceConfig.getMaximumMemory());
        commonServer.register(new UserServiceHandler(SpringApplicationContext.getBean(UserBuz.class)));
        commonServer.register(new ExceptionHttpHandler());
        commonServer.initServlet(UserServiceConfig.getApiPort(), UserServiceConfig.getNumOfTasks(), UserServiceConfig.getTimeout(), TimeUnit.SECONDS);
        commonServer.startServer();
    }
}
