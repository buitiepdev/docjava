package com.fts.be.training.service.a.config;

import com.fts.be.training.service.a.buz.UserBuz;
import com.fts.be.training.service.a.buz.impl.UserBuzImpl;
import com.fts.be.training.service.a.dao.UserDao;
import com.fts.be.training.service.a.dao.impl.UserDaoImpl;
import com.fts.be.training.service.a.handler.UserServiceHandler;
import com.fts.be.training.service.a.service.UserService;
import com.fts.be.training.service.a.service.impl.UserServiceImpl;
import com.fts.be.training.service.c.sdk.ClientGrpcSdk;
import com.fts.common.configuration.sdk.config.annotation.ConfigField;
import com.fts.common.configuration.sdk.config.annotation.ConfigService;
import com.fts.common.configuration.sdk.entity.impl.GenUidResourceConfig;
import com.fts.common.configuration.sdk.entity.impl.ServiceConfig;
import com.fts.dbservice.sdk.anotation.TransactionalProxyFactory;
import com.fts.dbservice.sdk.api.DBClient;
import com.fts.dbservice.sdk.client.ServiceFactory;
import com.fts.ioz.common.genuid.GenUID;
import com.fts.ioz.common.genuid.impl.GenUIDImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;

@Configuration
public class UserServiceConfig {
    @ConfigService("kz_user_dbservice")
    private static ServiceConfig dbClient;
    @ConfigService("iozf_genuid")
    private static GenUidResourceConfig genUID;
    @ConfigField("maximum_memory")
    private static double maximumMemory;
    @ConfigField("api_port")
    private static int apiPort;
    @ConfigField("number_tasks")
    private static int numOfTasks;
    @ConfigField("time_unit")
    private static String timeUnit;
    @ConfigField("timeout")
    private static int timeout;

    @ConfigField("host")
    private static String grpcHost;

    @ConfigField("grpc_port")
    private static int grpcPort;

    @Bean
    public GenUID genUID() throws SQLException {
        return new GenUIDImpl(genUID.getUrl(), genUID.getUsername(), genUID.getPassword());
    }

    @Bean
    public DBClient dbClient() {
        return ServiceFactory.getDBClient(dbClient.getHost(), dbClient.getGrpcPort(), 1);
    }

    @Bean
    public UserDaoImpl userDaoImpl(DBClient client) {
        return new UserDaoImpl(client);
    }
    @Bean
    public UserDao userDao(){
        return new UserDaoImpl(dbClient());
    }

    @Bean
    public UserServiceImpl userServiceImpl(UserDao userDao) {
        return new UserServiceImpl(userDao);
    }
    @Bean
    public UserService userService(){
        return TransactionalProxyFactory.newInstance(new UserServiceImpl(userDao()));
    }

    @Bean
    public UserBuzImpl userBuzImpl(UserService userService, GenUID genUID) {
        return new UserBuzImpl(userService, genUID);
    }

    @Bean
    public UserServiceHandler userServiceHandler(UserBuz userBuz) {
        return new UserServiceHandler(userBuz);
    }

    public static double getMaximumMemory() {
        return maximumMemory;
    }

    public static int getApiPort() {
        return apiPort;
    }

    public static int getNumOfTasks() {
        return numOfTasks;
    }

    public static String getTimeUnit() {
        return timeUnit;
    }

    public static int getTimeout() {
        return timeout;
    }

    public static String getGrpcHost() {
        return grpcHost;
    }

    public static int getGrpcPort() {
        return grpcPort;
    }
}
