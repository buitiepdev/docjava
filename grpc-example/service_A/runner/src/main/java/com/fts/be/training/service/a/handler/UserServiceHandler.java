package com.fts.be.training.service.a.handler;


import com.fts.be.training.service.a.buz.UserBuz;
import com.fts.be.training.service.a.representation.request.BaseHttpRequest;
import com.fts.be.training.service.a.representation.response.BaseHttpResponse;
import com.fts.be.training.service.c.presentation.response.BaseGrpcResponse;
import com.fts.be.training.service.c.sdk.ClientGrpcSdk;
import com.fts.common.api.common.constant.Protocol;
import com.fts.common.api.common.payload.data.DataResponse;
import com.fts.common.api.server.service.ServiceHandler;
import com.fts.common.api.server.service.annotation.HandlerService;
import com.fts.common.api.server.service.annotation.HandlerServiceClass;
import com.fts.ioz.common.genuid.GenUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;


@HandlerServiceClass
public class UserServiceHandler extends ServiceHandler {
    private static final Logger logger = LoggerFactory.getLogger(ClientGrpcSdk.class);
    private final ClientGrpcSdk client = new ClientGrpcSdk("localhost", 8081);
    private final UserBuz userBuz;

    public UserServiceHandler(UserBuz userBuz) {
        this.userBuz = userBuz;
    }

    @HandlerService(path = "/create", proto = Protocol.POST)
    public DataResponse<BaseHttpResponse> createUser(BaseHttpRequest request){
        logger.info("Receive CREATE_USER Message {}", request);
        BaseHttpResponse response = userBuz.register(request);
        DataResponse<BaseGrpcResponse> baseGrpcResponse = client.sendCreateWallet("create wallet", response.getUserId());
        logger.info(baseGrpcResponse.toString());
        return new DataResponse<>(response);
    }
}
