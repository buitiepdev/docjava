package com.fts.be.training.service.a.handler;

import com.fts.be.training.service.a.exception.HttpExceptionExample;
import com.fts.common.api.common.payload.message.BaseMessageResponse;
import com.fts.common.api.server.exception.ExceptionHandler;
import com.fts.common.api.server.exception.annotation.HandlerException;
import com.fts.common.api.server.exception.annotation.HandlerExceptionClass;

@HandlerExceptionClass
public class ExceptionHttpHandler extends ExceptionHandler {
    @HandlerException
    public BaseMessageResponse exampleException(HttpExceptionExample e) {
        String message = e.getMessage() != null ? e.getMessage() : e.toString();
        int code = -1;
        int status = 500;
        return new BaseMessageResponse(message,code, status);
    }
}
