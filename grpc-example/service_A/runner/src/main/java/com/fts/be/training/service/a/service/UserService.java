package com.fts.be.training.service.a.service;

import com.fts.be.training.service.a.entity.UserEntity;

public interface UserService {
    UserEntity register(Long userId,String username, String email, String phone, String password);
}
