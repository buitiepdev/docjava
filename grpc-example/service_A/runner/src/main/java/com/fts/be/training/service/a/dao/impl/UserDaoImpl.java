package com.fts.be.training.service.a.dao.impl;

import com.fts.be.training.service.a.dao.UserDao;
import com.fts.be.training.service.a.entity.UserEntity;
import com.fts.be.training.service.c.sdk.ClientGrpcSdk;
import com.fts.dbservice.sdk.api.DBClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserDaoImpl implements UserDao {
    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    private final DBClient client;

    public UserDaoImpl(DBClient client) {
        this.client = client;
    }

    // query db
    private static final String INSERT_USER = "INSERT INTO :user_bt_demo: (id, username, email, password, phone_number) VALUES (%d, '%s','%s', '%s', '%s')";

    @Override
    public void register(UserEntity userEntity) {
        String query = String.format(INSERT_USER, userEntity.getUserId(), userEntity.getUsername(), userEntity.getEmail(), userEntity.getPhone(), userEntity.getPassword());
        logger.info(query);
        client.executeV2(query, Long.toString(userEntity.getUserId()));
    }
}
