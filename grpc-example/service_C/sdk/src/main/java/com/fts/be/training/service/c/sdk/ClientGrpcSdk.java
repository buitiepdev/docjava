package com.fts.be.training.service.c.sdk;


import com.fts.be.training.service.c.presentation.constant.MethodConstant;
import com.fts.be.training.service.c.presentation.request.BaseGrpcRequest;
import com.fts.be.training.service.c.presentation.response.BaseGrpcResponse;
import com.fts.be.training.service.c.sdk.impl.GrpcSdk;
import com.fts.common.api.client.service.AbstractService;
import com.fts.common.api.common.payload.data.DataResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientGrpcSdk extends AbstractService implements GrpcSdk {
    private static final Logger logger = LoggerFactory.getLogger(ClientGrpcSdk.class);
    public ClientGrpcSdk(String host, int port) {
        super(host, port);
    }
    public DataResponse<BaseGrpcResponse> sendCreateWallet(String message, Long data) {
        logger.info(data.toString());
        BaseGrpcRequest request = new BaseGrpcRequest(message, data);
        return client.sendRequest("", MethodConstant.METHOD_GET, request);
    }
    public DataResponse<BaseGrpcResponse> postExample(String message, Long data) {
        logger.info(data.toString());
        BaseGrpcRequest request = new BaseGrpcRequest(message, data);
        return client.sendRequest("", MethodConstant.METHOD_POST, request);
    }
}
