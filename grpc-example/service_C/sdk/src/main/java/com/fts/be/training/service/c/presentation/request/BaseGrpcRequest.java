package com.fts.be.training.service.c.presentation.request;

import com.fts.common.api.common.payload.data.impl.BaseDataRequest;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseGrpcRequest extends BaseDataRequest {
    // define version serialize
    private static final long serialVersionUID = 1L;
    @SerializedName("request_id")
    private String requestId;
    @SerializedName("message")
    private String message;

    @SerializedName("input")
    private Long input;

    public BaseGrpcRequest(String message,Long input) {
        this.message = message;
        this.input = input;
    }


    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }


    public void setMessage(String message) {
        this.message = message;
    }

    public Long getInput() {
        return input;
    }

    public void setInput(Long input) {
        this.input = input;
    }

    public String getMessage() {
        return message;
    }


    @Override
    public String toString() {
        return "BaseGrpcRequest{" +
                "requestId='" + requestId + '\'' +
                ", message='" + message + '\'' +
                ", input=" + input +
                '}';
    }
}
