package com.fts.be.training.service.c.dao.impl;

import com.fts.be.training.service.c.dao.WalletDao;
import com.fts.be.training.service.c.entity.WalletEntity;
import com.fts.dbservice.sdk.api.DBClient;

public class WalletDaoImpl implements WalletDao {
    private final DBClient client;

    public WalletDaoImpl(DBClient client) {
        this.client = client;
    }

    private static final String INSERT_WALLET = "INSERT INTO :wallet_bt_demo: (wallet_id, user_id, balance, created_at, updated_at) VALUES (%d, '%d','%f', '%d', '%d')";
    @Override
    public void create(WalletEntity walletEntity) {
        String query = String.format(INSERT_WALLET, walletEntity.getWalletId(), walletEntity.getUserId(), walletEntity.getBalance(), walletEntity.getCreatedAt(), walletEntity.getUpdatedAt());
        client.executeV2(query, Long.toString(walletEntity.getUserId()));
    }
}
