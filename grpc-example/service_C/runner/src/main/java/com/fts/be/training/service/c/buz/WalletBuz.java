package com.fts.be.training.service.c.buz;

import com.fts.be.training.service.c.presentation.request.BaseGrpcRequest;
import com.fts.be.training.service.c.presentation.response.BaseGrpcResponse;
public interface WalletBuz {
    BaseGrpcResponse create(BaseGrpcRequest request);
}
