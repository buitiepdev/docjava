package com.fts.be.training.service.c.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WalletEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("wallet_id")
    private Long walletId;

    @SerializedName("user_id")
    private Long userId;

    @SerializedName("balance")
    private Double balance;
    @SerializedName("created_at")
    private Long createdAt;

    @SerializedName("updated_at")
    private Long updatedAt;

    public Long getWalletId() {
        return walletId;
    }

    public Long getUserId() {
        return userId;
    }

    public double getBalance() {
        return balance;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "WalletEntity{" +
                "walletId=" + walletId +
                ", userId=" + userId +
                ", balance=" + balance +
                '}';
    }
}
