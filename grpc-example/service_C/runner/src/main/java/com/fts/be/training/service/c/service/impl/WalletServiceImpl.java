package com.fts.be.training.service.c.service.impl;

import com.fts.be.training.service.c.dao.WalletDao;
import com.fts.be.training.service.c.entity.WalletEntity;
import com.fts.be.training.service.c.service.WalletService;
import com.fts.dbservice.sdk.anotation.Transactional;
import com.fts.ioz.common.genuid.GenUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WalletServiceImpl implements WalletService {
    private static final Logger logger = LoggerFactory.getLogger(WalletServiceImpl.class);
    private final WalletDao walletDao;
    public WalletServiceImpl(WalletDao walletDao) {
        this.walletDao = walletDao;
    }

    @Transactional
    @Override
    public WalletEntity create(Long walletId, Long userId, Double balance, Long createdAt, Long updatedAt) {
        WalletEntity wallet = new WalletEntity();
        wallet.setWalletId(walletId);
        wallet.setUserId(userId);
        wallet.setBalance(balance);
        wallet.setCreatedAt(createdAt);
        wallet.setUpdatedAt(updatedAt);
        this.walletDao.create(wallet);
        return wallet;
    }
}
