package com.fts.be.training.service.c.service;

import com.fts.be.training.service.c.entity.WalletEntity;

public interface WalletService {
    WalletEntity create(Long walletId, Long userId, Double balance, Long createdAt, Long updatedAt);
}
