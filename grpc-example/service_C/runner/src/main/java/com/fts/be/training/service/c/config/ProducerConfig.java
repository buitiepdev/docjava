package com.fts.be.training.service.c.config;


import com.fts.be.training.service.b.representation.constant.KafkaConstant;
import com.fts.be.training.service.b.representation.msg.KafkaMessage;
import com.fts.be.training.service.c.producer.DemoProducer;
import com.fts.common.configuration.common.response.KafkaConfig;
import com.fts.common.configuration.sdk.config.annotation.ConfigKafkaResource;
import com.fts.common.extensions.annotation.EnableKafkaTracing;
import com.fts.common.extensions.producer.KafkaProducer;
import com.fts.common.extensions.producer.KafkaProducerConfigInit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

@Configuration
@EnableKafkaTracing
public class ProducerConfig {
    @ConfigKafkaResource(value = "kafka_topic", scope = "kafka_demo")
    public static KafkaConfig kafkaDemo;

    @Bean
    public KafkaTemplate<String, KafkaMessage> kafkaDemoMessageTemplate() {
        return KafkaProducerConfigInit.getKafkaRepKafkaTemplate(kafkaDemo.getTopicConfig(KafkaConstant.DEMO_TOPIC));
    }

    @Bean
    public KafkaProducer kafkaProducer() {
        return new KafkaProducer(null); // Bypass dbClient
    }

    @Bean
    public DemoProducer demoProducer(KafkaProducer kafkaProducer, KafkaTemplate<String, KafkaMessage> kafkaDemoMessageTemplate) {
        return new DemoProducer(kafkaProducer, kafkaDemoMessageTemplate);
    }
}
