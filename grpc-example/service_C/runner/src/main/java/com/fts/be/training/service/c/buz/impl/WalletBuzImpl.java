package com.fts.be.training.service.c.buz.impl;

import com.fts.be.training.service.c.buz.WalletBuz;
import com.fts.be.training.service.c.presentation.request.BaseGrpcRequest;
import com.fts.be.training.service.c.presentation.response.BaseGrpcResponse;
import com.fts.be.training.service.c.service.WalletService;
import com.fts.ioz.common.genuid.GenUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WalletBuzImpl implements WalletBuz {
    private static final Logger logger = LoggerFactory.getLogger(WalletBuzImpl.class);
    private final WalletService walletService;
    private final GenUID genUID;

    public WalletBuzImpl(WalletService walletService, GenUID genUID) {
        this.walletService = walletService;
        this.genUID = genUID;
    }

    @Override
    public BaseGrpcResponse create(BaseGrpcRequest request) {
        logger.info(request.toString());

        BaseGrpcResponse response = new BaseGrpcResponse("Create Wallet", 1);
        return response;
    }
}
