package com.fts.be.training.service.c.config;

import com.fts.be.training.service.c.buz.WalletBuz;
import com.fts.be.training.service.c.buz.impl.WalletBuzImpl;
import com.fts.be.training.service.c.dao.WalletDao;
import com.fts.be.training.service.c.dao.impl.WalletDaoImpl;
import com.fts.be.training.service.c.handler.WalletServiceHandler;
import com.fts.be.training.service.c.producer.DemoProducer;
import com.fts.be.training.service.c.service.WalletService;
import com.fts.be.training.service.c.service.impl.WalletServiceImpl;
import com.fts.common.configuration.sdk.config.annotation.ConfigField;
import com.fts.common.configuration.sdk.config.annotation.ConfigService;
import com.fts.common.configuration.sdk.entity.impl.GenUidResourceConfig;
import com.fts.common.configuration.sdk.entity.impl.ServiceConfig;
import com.fts.dbservice.sdk.api.DBClient;
import com.fts.dbservice.sdk.client.ServiceFactory;
import com.fts.ioz.common.genuid.GenUID;
import com.fts.ioz.common.genuid.impl.GenUIDImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;

@Configuration
public class WalletServiceConfig {

    @ConfigService("kz_user_dbservice")
    private static ServiceConfig dbClient;
    @ConfigService("iozf_genuid")
    private static GenUidResourceConfig genUID;
    @ConfigField("maximum_memory")
    private static double maximumMemory;
    @ConfigField("api_port")
    private static int apiPort;
    @ConfigField("number_tasks")
    private static int numOfTasks;
    @ConfigField("time_unit")
    private static String timeUnit;
    @ConfigField("timeout")
    private static int timeout;

    @ConfigField("host")
    private static String grpcHost;

    @ConfigField("grpc_port")
    private static int grpcPort;

    @Bean
    public GenUID genUID() throws SQLException {
        return new GenUIDImpl(genUID.getUrl(), genUID.getUsername(), genUID.getPassword());
    }

    @Bean
    public DBClient dbClient() {
        return ServiceFactory.getDBClient(dbClient.getHost(), dbClient.getGrpcPort(), 1);
    }
    @Bean
    public WalletBuz walletBuz(WalletService walletService, GenUID genUID) {
        return new WalletBuzImpl(walletService, genUID);
    }
    @Bean
    public WalletServiceHandler walletServiceHandler(DemoProducer producer, WalletBuz walletBuz) {
        return new WalletServiceHandler(producer, walletBuz);
    }
    @Bean
    public WalletDao walletDao() {
        return new WalletDaoImpl(dbClient());
    }
    @Bean
    public WalletDaoImpl walletDaoImpl(DBClient client) {
        return new WalletDaoImpl(client);
    }
    @Bean
    public WalletServiceImpl walletServiceImpl(WalletDao walletDao) {
        return new WalletServiceImpl(walletDao);
    }
}
