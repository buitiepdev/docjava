package com.fts.be.training.service.c.dao;

import com.fts.be.training.service.c.entity.WalletEntity;

public interface WalletDao {
    void create(WalletEntity walletEntity);
}
