package com.fts.be.training.service.c;

import com.fts.be.training.service.c.config.ProducerConfig;
import com.fts.be.training.service.c.config.WalletServiceConfig;
import com.fts.be.training.service.c.handler.WalletExceptionHandler;
import com.fts.be.training.service.c.handler.WalletServiceHandler;
import com.fts.common.api.server.CommonServer;
import com.fts.common.configuration.sdk.config.InitConfiguration;
import com.fts.ioz.common.ioc.SpringApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import java.util.concurrent.TimeUnit;

public class WalletServiceRunner {

    public static void main(String[] args) throws Exception {

        InitConfiguration initConfiguration = new InitConfiguration("demo.producer.application.properties");
        initConfiguration.setConfigurationAnnotation(ProducerConfig.class, WalletServiceConfig.class);
        AnnotationConfigApplicationContext context = initConfiguration.getContext();
        context.refresh();
        SpringApplicationContext.setSharedApplicationContext(context);

        CommonServer commonServer = new CommonServer(
                90D
        );
        commonServer.register(SpringApplicationContext.getBean(WalletServiceHandler.class));
        commonServer.register(new WalletExceptionHandler());
        commonServer.initGrpc(8081, false, 10, 60, TimeUnit.SECONDS);
        commonServer.initServlet(8082, 10, 60, TimeUnit.SECONDS);
        commonServer.startServer();
        Thread.currentThread().join();
    }
}
