package com.fts.be.training.dbservice.runner.config;


import com.fts.be.training.dbservice.runner.buz.UserBuz;
import com.fts.be.training.dbservice.runner.buz.impl.UserBuzImpl;
import com.fts.be.training.dbservice.runner.dao.UserDao;
import com.fts.be.training.dbservice.runner.dao.impl.UserDaoImpl;
import com.fts.be.training.dbservice.runner.handler.UserHandler;
import com.fts.be.training.dbservice.runner.service.UserService;
import com.fts.be.training.dbservice.runner.service.impl.UserServiceImpl;
import com.fts.common.configuration.sdk.config.annotation.ConfigService;
import com.fts.common.configuration.sdk.entity.impl.GenUidResourceConfig;
import com.fts.common.configuration.sdk.entity.impl.ResourceConfig;
import com.fts.common.configuration.sdk.entity.impl.ServiceConfig;
import com.fts.dbservice.sdk.anotation.TransactionalProxyFactory;
import com.fts.dbservice.sdk.api.DBClient;
import com.fts.dbservice.sdk.client.ServiceFactory;
import com.fts.ioz.common.genuid.GenUID;
import com.fts.ioz.common.genuid.impl.GenUIDImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;

@Configuration
public class ServiceDBConfig {
    @ConfigService("kz_user_dbservice")
    private static ServiceConfig dbClient;

    @ConfigService("iozf_genuid")
    private static GenUidResourceConfig genUID;
    @Bean
    public GenUID genUID() throws SQLException {
        return new GenUIDImpl(genUID.getUrl(), genUID.getUsername(), genUID.getPassword());
    }

    @Bean
    public DBClient dbClient() {
        return ServiceFactory.getDBClient(dbClient.getHost(), dbClient.getGrpcPort(), 1);
    }

    @Bean
    public UserDao userDao(){
        return new UserDaoImpl(dbClient());
    }
    @Bean
    public UserService userService(){
        return TransactionalProxyFactory.newInstance(new UserServiceImpl(userDao()));
    }
    @Bean
    public UserBuzImpl userBuzImpl(UserService userService, GenUID genUID) {
        return new UserBuzImpl(userService, genUID);
    }
    @Bean
    public UserDaoImpl userDaoImpl(DBClient client) {
        return new UserDaoImpl(client);
    }

    @Bean
    public UserHandler userHandler(UserBuz userBuz) {
        return new UserHandler(userBuz);
    }
}
