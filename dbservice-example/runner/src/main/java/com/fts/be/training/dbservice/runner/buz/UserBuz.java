package com.fts.be.training.dbservice.runner.buz;

import com.fts.be.training.dbservice.runner.representation.BaseRequest;
import com.fts.be.training.dbservice.runner.representation.BaseResponse;
import com.fts.common.api.common.payload.data.DataResponse;
import com.fts.dbservice.sdk.exception.DBClusterException;

public interface UserBuz {
    BaseResponse getUser(Long userId) throws DBClusterException;
    BaseResponse create(BaseRequest request) throws DBClusterException;
}
