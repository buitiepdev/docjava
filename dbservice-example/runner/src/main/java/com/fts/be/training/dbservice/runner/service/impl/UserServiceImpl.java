package com.fts.be.training.dbservice.runner.service.impl;

import com.fts.be.training.dbservice.runner.dao.UserDao;
import com.fts.be.training.dbservice.runner.entity.User;
import com.fts.be.training.dbservice.runner.service.UserService;
import com.fts.dbservice.sdk.anotation.Transactional;
import com.fts.dbservice.sdk.exception.DBClusterException;
import com.fts.ioz.common.genuid.GenUID;

public class UserServiceImpl implements UserService {
    private final UserDao userDao;


    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public User create(Long userId, String email, String phone, String password) {
        User user = new User();
        user.setUserId(userId);
        user.setEmail(email);
        user.setPhone(phone);
        user.setPassword(password);
        this.userDao.create(user);
        return user;
    }

    @Override
    @Transactional
    public User getUser(Long userId) throws DBClusterException {
        return this.userDao.getUser(userId);
    }

}
