package com.fts.be.training.dbservice.runner.representation;

import com.fts.common.api.common.payload.data.impl.BaseDataRequest;
import com.google.gson.annotations.SerializedName;

public class BaseRequest extends BaseDataRequest {
    private static final long serialVersionUID = 1L;

    @SerializedName("user_id")
    private Long userId;
    @SerializedName("user_email")
    private String email;
    @SerializedName("user_phone")
    private String phone;
    @SerializedName("user_password")
    private String password;

    public Long getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "BaseRequest{" +
                "userId=" + userId +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
