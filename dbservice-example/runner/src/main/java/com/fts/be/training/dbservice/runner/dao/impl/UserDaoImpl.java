package com.fts.be.training.dbservice.runner.dao.impl;


import com.fts.be.training.dbservice.runner.dao.UserDao;
import com.fts.be.training.dbservice.runner.entity.User;
import com.fts.dbservice.sdk.api.DBClient;
import com.fts.dbservice.sdk.exception.DBClusterException;

import java.util.List;

public class UserDaoImpl implements UserDao {

    private final DBClient client;
    private static final String GET_USER = "SELECT user_id, user_phone, user_email FROM :user_demo: WHERE user_id = %d";
    private static final String INSERT_USER_PROFILE = "INSERT INTO :user_demo: (user_id, user_email, user_phone, user_password) VALUES (%d, '%s','%s', '%s')";
    public UserDaoImpl (DBClient client){
        this.client = client;
    }
    @Override
    public void create(User user) {
        String query = String.format(INSERT_USER_PROFILE, user.getUserId(), user.getEmail(), user.getPhone(), user.getPassword());
        client.executeV2(query, Long.toString(user.getUserId()));
    }

    @Override
    public void update(User user) {

    }

    @Override
    public User getUser(Long userId) throws DBClusterException {
        String query = String.format(GET_USER, userId);
        List<User> user = client.queryV2(query, Long.toString(userId), User.class);
        if (user == null || user.isEmpty()) {
            user = client.queryV2(query,Long.toString(userId), User.class);
            if(user == null || user.isEmpty()){
                return null;
            }
        }
        return user.get(0);
    }
}
