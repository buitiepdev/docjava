package com.fts.be.training.dbservice.runner.dao;

import com.fts.be.training.dbservice.runner.entity.User;
import com.fts.dbservice.sdk.exception.DBClusterException;

public interface UserDao {
    void create(User user);
    void update(User user);

    User getUser(Long userId) throws DBClusterException;
}
