package com.fts.be.training.dbservice.runner.handler;

import com.fts.be.training.dbservice.runner.buz.UserBuz;
import com.fts.be.training.dbservice.runner.constant.UserMethod;
import com.fts.be.training.dbservice.runner.representation.BaseRequest;
import com.fts.be.training.dbservice.runner.representation.BaseResponse;
import com.fts.common.api.common.constant.Protocol;
import com.fts.common.api.common.payload.data.DataResponse;
import com.fts.common.api.server.service.ServiceHandler;
import com.fts.common.api.server.service.annotation.HandlerService;
import com.fts.common.api.server.service.annotation.HandlerServiceClass;
import com.fts.common.api.server.utils.Parser;
import com.fts.dbservice.sdk.exception.DBClusterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@HandlerServiceClass
public class UserHandler extends ServiceHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserHandler.class);

    private final UserBuz userBuz;

    public UserHandler(UserBuz userBuz) {
        this.userBuz = userBuz;
    }
    @HandlerService(method = UserMethod.CREATE_USER, path = "/create", proto = Protocol.POST)
    public DataResponse<BaseResponse> createUser(BaseRequest request) throws DBClusterException {
        LOGGER.info("Receive CREATE_USER Message {}", request);
        BaseResponse response = userBuz.create(request);
        return new DataResponse<>(response);
    }
    @HandlerService(path = "/get", proto = Protocol.GET)
    public DataResponse<BaseResponse> getUser(BaseRequest request) throws DBClusterException {
        LOGGER.info("Receive CREATE_USER Message {}", request);
        BaseResponse response = userBuz.getUser(request.getUserId());
        return new DataResponse<>(response);
    }
}
