package com.fts.be.training.dbservice.runner.buz.impl;

import com.fts.be.training.dbservice.runner.buz.UserBuz;
import com.fts.be.training.dbservice.runner.entity.User;
import com.fts.be.training.dbservice.runner.representation.BaseRequest;
import com.fts.be.training.dbservice.runner.representation.BaseResponse;
import com.fts.be.training.dbservice.runner.service.UserService;
import com.fts.dbservice.sdk.exception.DBClusterException;
import com.fts.ioz.common.genuid.GenUID;

public class UserBuzImpl implements UserBuz {
    private final UserService userService;
    private final GenUID genUID;


    public UserBuzImpl(UserService userService, GenUID genUID) {
        this.userService = userService;
        this.genUID = genUID;
    }


    @Override
    public BaseResponse getUser(Long userId) throws DBClusterException {
        User user = null;
        user = userService.getUser(userId);
        BaseResponse response = new BaseResponse();
        response.setEmail(user.getEmail());
        return response;
    }

    @Override
    public BaseResponse create(BaseRequest request) throws DBClusterException {
        Long userIdGen = genUID.getUID();
        User user = userService.create(userIdGen, request.getEmail(), request.getPhone(), request.getPassword());
        BaseResponse response = new BaseResponse();
        response.setUserId(user.getUserId());
        response.setPhone(user.getPhone());
        return response;
    }
}
