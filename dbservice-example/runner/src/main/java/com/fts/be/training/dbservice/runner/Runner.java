package com.fts.be.training.dbservice.runner;

import com.fts.be.training.dbservice.runner.buz.UserBuz;
import com.fts.be.training.dbservice.runner.config.ServiceDBConfig;
import com.fts.be.training.dbservice.runner.handler.UserExceptionHandler;
import com.fts.be.training.dbservice.runner.handler.UserHandler;
import com.fts.common.api.server.CommonServer;
import com.fts.common.configuration.sdk.config.InitConfiguration;
import com.fts.ioz.common.ioc.SpringApplicationContext;
import org.eclipse.jetty.server.Authentication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.concurrent.TimeUnit;

public class Runner {
    public static void main(String[] args)  throws Exception  {
        InitConfiguration initConfiguration = new InitConfiguration();
        initConfiguration.setConfigurationAnnotation(ServiceDBConfig.class);
        AnnotationConfigApplicationContext context = initConfiguration.getContext();
        context.refresh();
        SpringApplicationContext.setSharedApplicationContext(context);
        CommonServer commonServer = new CommonServer(90D);
        commonServer.register(new UserHandler(SpringApplicationContext.getBean(UserBuz.class)));
        commonServer.register(new UserExceptionHandler());
        commonServer.initServlet(8080, 10, 60, TimeUnit.SECONDS);
        commonServer.startServer();
    }
}