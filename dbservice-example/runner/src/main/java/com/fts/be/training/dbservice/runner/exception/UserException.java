package com.fts.be.training.dbservice.runner.exception;

public class UserException extends Exception{
    public UserException() {
    }

    public UserException(String s) {
        super(s);
    }

    public UserException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
