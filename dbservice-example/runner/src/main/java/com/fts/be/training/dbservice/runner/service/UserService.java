package com.fts.be.training.dbservice.runner.service;

import com.fts.be.training.dbservice.runner.entity.User;
import com.fts.dbservice.sdk.exception.DBClusterException;

public interface UserService {
    User create(Long userId, String email, String phone, String password);
    User getUser(Long userId) throws DBClusterException;
}
